#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <stdlib.h>
int main(int argc, char* argv[], char* envp[]) 
{
  
  pid_t pid=fork();
  int stc;

  if(pid==0) { //Kind prozess
    printf("[K]MEIN PID : %d \n",getpid());
    execv(argv[1],&argv[1]);
    exit(1);
  }
  if(pid>0) {
    printf("[E]MEIN PID : %d \n",getpid());
    printf("[E]KIND PID : %d \n",pid);
    wait(&stc);
  }

  if (WIFEXITED(stc)) {
    printf("[E]Kind prozess mit %d beendet \n", WEXITSTATUS(stc));
  }
}
